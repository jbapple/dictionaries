#ifndef SUCCINCTRANDOMINTHASH_HPP
#define SUCCINCTRANDOMINTHASH_HPP

#include <cstdint>
#include <memory>
#include <cassert>
#include <iostream>

using namespace std;

//#include "SmoothRandomIntHash.hpp"

#ifdef NDEBUG
#undef assert
#define assert(x) (void)(x)
#endif

// SuccinctRandomIntHash is a hash set for random integers. If the inserted
// integers are insufficiently random, it will perform poorly.
class SuccinctRandomIntHash {

public:
  unsigned int capacity_;
  unsigned int capacity_mask_;
  unsigned int log_leaf_capacity_;
  unsigned int leaf_capacity_;
  unsigned int leaf_mask_;
  unsigned int size_;
  int ** root_;
  bool default_is_member_;
  
  struct todo {
    int ** top_;
    unsigned int top_size_;
    unsigned int bottom_size_;
    unsigned int progress_;
    // returns true is deallocs
    bool go() {
      if (progress_ > top_size_) return false;
      if (progress_ == top_size_) {
        delete[] top_;
        ++progress_;
        return true;
      }
      delete[] top_[progress_++];
      return true;
    }
    ~todo() {
      while (go());
    }
  };
  
  todo * old;

  
  // MAX_LOAD is the number of slots there must be for every data item

  SuccinctRandomIntHash(const unsigned int root_capacity = 1, 
                        const unsigned int log_leaf_capacity = 1) 
    : capacity_((1u << log_leaf_capacity) * root_capacity),
      capacity_mask_(capacity_ - 1),
      log_leaf_capacity_(log_leaf_capacity),
      leaf_capacity_(1 << log_leaf_capacity_),
      leaf_mask_(leaf_capacity_ - 1),
      size_(0),
      root_(new int*[root_capacity]()),
      default_is_member_(false),
      old(nullptr)
  {}

  int * get_nth(const unsigned int i) {
    if (nullptr == root_[i >> log_leaf_capacity_]) {
      return nullptr;
    }

    int * const ans = &root_[i >> log_leaf_capacity_][i & leaf_mask_];
    
    if (int() == *ans) {
      return nullptr;
    }

    return ans;
  }
  
  // returns true if alloced
  bool set_nth(const unsigned int i, const int x) {
    bool ans = false;
    if (nullptr == root_[i >> log_leaf_capacity_]) {
      /*
      cerr << "hard block " 
           << size_ << '\t'
           << capacity_ << endl;
      */
      root_[i >> log_leaf_capacity_] = new int[leaf_capacity_]();//INT_ALLOC.allocate(leaf_capacity_);
      //for (unsigned int j = 0; j < leaf_capacity_; ++j) {
      //  root_[i >> log_leaf_capacity_][j] = int();
      //}
      ans = true;
    } else {
      if (old) {
        ans = old->go();
      }
    }

    root_[i >> log_leaf_capacity_][i & leaf_mask_] = x;
    return ans;
  }


  // EasyInsert performs the insert only if it does not require a
  // resize. It returns -1 on failure and otherwise the location of
  // the item inserted, or capacity_ if item was int() or already
  // present.
  //
  // first is true if alloc was performed
  pair<bool,int> EasyInsert(const int item) {

    if (item == int()) {
      if (not default_is_member_) {
        ++size_;
      }
      default_is_member_ = true;
      return make_pair(false,capacity_);
    }

    for (unsigned int i = 0; i < capacity_; ++i) {

      const unsigned int j = ((item & (capacity_mask_)) + i) & (capacity_mask_);
      { 
        // don't accidentally use i as an index;
        static void (*const i)() = nullptr;
        (void)(i);

        const int * const jth = get_nth(j);

        if (nullptr != jth) {
          if (*jth == item) { return make_pair(false,capacity_); }
          continue;
        }

        if (size_ * MAX_LOAD >= capacity_) { return make_pair(false,-1); }

        ++size_;
        const bool alloc = set_nth(j, item);
        return make_pair(alloc,j);
      }
    }
    assert (MAX_LOAD * (size_ - 1) < capacity_);
    return make_pair(false,-1);
  }

  /*
  bool HardInsert(const int item) {
    if (EasyInsert(item)) { return true; }

    const unsigned int root_capacity = capacity_ >> log_leaf_capacity_;
    
    unsigned int new_root_capacity = root_capacity;
    unsigned int new_log_leaf_capacity = 1 + log_leaf_capacity_;

    if (root_capacity < (1u << log_leaf_capacity_)) {
      new_root_capacity *= 2;
      --new_log_leaf_capacity;
    }

    SuccinctRandomIntHash that(new_root_capacity, new_log_leaf_capacity);
    if (default_is_member_) {
      that.default_is_member_ = true;
    }
    for (unsigned int i = 0; i < capacity_; ++i) {
      const int * const x = get_nth(i);
      if (x) {
        const bool res = that.EasyInsert(*x);
        assert(res && "Failed insert on resize");
      }
    }
    that.EasyInsert(item);
    swap_with(that);
    return false;
  }
  */

  ~SuccinctRandomIntHash() {
    for (unsigned int i = 0; i < (capacity_ >> log_leaf_capacity_); ++i) {
      delete[] root_[i];////INT_ALLOC.deallocate(root_[i], leaf_capacity_);
    }
    delete[] root_;
    delete old;
  }

  unsigned int size() const { return size_; }
  unsigned int capacity() const { return capacity_; }  


public:

  void swap_with(SuccinctRandomIntHash & that) {
    std::swap(capacity_, that.capacity_);
    std::swap(capacity_mask_, that.capacity_mask_);
    std::swap(log_leaf_capacity_, that.log_leaf_capacity_);
    std::swap(leaf_capacity_, that.leaf_capacity_);
    std::swap(leaf_mask_, that.leaf_mask_);
    std::swap(size_, that.size_);
    std::swap(root_, that.root_);
    std::swap(default_is_member_, that.default_is_member_);
    std::swap(old, that.old);
  }

  // struct FreeOld : public Work {
  //   int ** old_root_;
  //   const unsigned int root_capacity_;
  //   const unsigned int leaf_capacity_;
  //   unsigned int cursor_;
  //   unsigned int counter_;

  //   FreeOld(int ** old_root, 
  //           const unsigned int root_capacity, 
  //           const unsigned int leaf_capacity) 
  //     : old_root_(old_root),
  //       root_capacity_(root_capacity),
  //       leaf_capacity_(leaf_capacity),
  //       cursor_(0),
  //       counter_(0) {}

  //   //virtual unsigned int units() { return root_capacity; }

  //   virtual void go() { /*
  //     ++counter_;
  //     if (counter_ != (leaf_capacity_ >> 4)) {
  //       return;
  //     }
  //     counter_ = 0;*/
  //     if(cursor_ < root_capacity_) {
  //       SuccinctRandomIntHash::INT_ALLOC.deallocate(old_root_[cursor_], leaf_capacity_);
  //       ++cursor_;
  //       if (cursor_ == root_capacity_) {
  //         delete[] old_root_;
  //       }
  //     }
  //   }

  //   virtual ~FreeOld() {
  //     assert (false);
  //     assert (cursor_ == root_capacity_);
  //   }

  // };


  void reset() {
    //Work * ans = new FreeOld(root_, capacity_ >> log_leaf_capacity_, leaf_capacity_);

    if (old) {
      while (old->go());
    }
    
    if (nullptr == old) {
      old = new todo();
    }
    old->top_ = root_;
    old->top_size_ = (capacity_ >> log_leaf_capacity_);
    old->bottom_size_ = leaf_capacity_;
    old->progress_ = 0;

    /*
    for (unsigned int i = 0; i < (capacity_ >> log_leaf_capacity_); ++i) {
      INT_ALLOC.deallocate(root_[i], leaf_capacity_);
    }
    delete[] root_;
    */


    const int root_capacity = 2 * (capacity_ >> log_leaf_capacity_);
    ++log_leaf_capacity_;
    capacity_ = (1u << log_leaf_capacity_) * root_capacity;
    capacity_mask_ = capacity_ - 1;
    leaf_capacity_ = 1 << log_leaf_capacity_;
    leaf_mask_ = leaf_capacity_ - 1;
    size_ = 0;
    /*
    cerr << "new root: " 
         << root_capacity << '\t'
         << capacity_ << endl;
    */
    root_ = new int*[root_capacity]();
    default_is_member_ = false;

    

    //return ans;

  }

private:

  
  static std::allocator<int> INT_ALLOC;

public:
  const static unsigned int MAX_LOAD = 2;

};

/*static*/ std::allocator<int> SuccinctRandomIntHash::INT_ALLOC = std::allocator<int>();


#endif
