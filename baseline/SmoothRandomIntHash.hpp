#ifndef SMOOTHRANDOMINTHASH_HPP
#define SMOOTHRANDOMINTHASH_HPP

#include "RandomIntHash.hpp"

#include <unordered_set>
using namespace std;

struct Work {
  virtual void go() = 0;
  //virtual ~Work() = 0;
};

template<typename H>
struct SmoothRandomIntHash {
  
  SmoothRandomIntHash() : progress_(0), smaller_(2), bigger_(4), worker_(nullptr) {}//, reference_(1) {}

  unsigned int progress_;
  H smaller_;
  H bigger_;
  Work * worker_;
  //unordered_set<int> reference_;

  void one_step() {
    const unsigned int top = progress_ + 32;
    for (; (progress_ < smaller_.capacity()) and (progress_ < top); ++progress_) {
      const int * x = smaller_.get_nth(progress_);
      if (x) {
        const pair<bool,int> res = bigger_.EasyInsert(*x);
        assert (res.second > -1);
	if (res.first) { return; }
      }
    }
    //if (worker_) {
    //  worker_->go();
    //}
  }

  void insert(const int item) {
    //reference_.insert(item);
    const pair<bool,int> res = smaller_.EasyInsert(item);
    if (not res.first) {
      one_step();
    }
    if ((-1 == res.second) or (static_cast<unsigned>(res.second) <= progress_)) {
      const pair<bool,int> lr = bigger_.EasyInsert(item);
      assert ((lr.second != -1) && "Insert failed on larger set");
    }
    if (-1 == res.second) {
      while (progress_ < smaller_.capacity()) {
	//cerr << "uh oh\t" << progress_ << '\t' << smaller_.capacity() << endl;
	one_step();
      }
      assert (progress_ >= smaller_.capacity());
      smaller_.swap_with(bigger_);
      //delete worker_;
      //worker_ = 
      bigger_.reset();
      progress_ = 0;
      //if (smaller_.default_is_member_) {
      //bigger_.default_is_member_ = true;
      //}
      //one_step();
      //cerr << "active " << smaller_.capacity() << endl;
      //assert (smaller_.same_set(reference_));
    }
  }

  ~SmoothRandomIntHash() {
    //for (;progress_ < smaller_.capacity(); ++ progress_) {
      //worker_->go();
    //}
    //delete worker_;
  }

};

#endif
