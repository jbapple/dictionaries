#ifndef RANDOMINTHASH_HPP
#define RANDOMINTHASH_HPP

#include <cstdint>
#include <memory>
#include <cassert>
#include <algorithm>

#ifdef NDEBUG
#undef assert
#define assert(x) (void)(x)
#endif

// RandomIntHash is a hash set for random integers. If the inserted
// integers are insufficiently random, it will perform poorly.
class RandomIntHash {

public:
  RandomIntHash(const unsigned int capacity = 1) 
    : capacity_(capacity), size_(0), 
      data_(INT_ALLOC.allocate(capacity_)), occupancy_(capacity_) 
  {}

  // EasyInsert performs the insert only if it does not require a
  // resize. It returns -1 on failure and otherwise the location of
  // the item inserted, or capacity_ if item was int() or already
  // present.
  int EasyInsert(const int item) {

    for (unsigned int i = ((item & (capacity_-1)) + capacity_) & (capacity_-1); 
         i < (((item & (capacity_-1)) + capacity_) & (capacity_-1)) + capacity_;
         ++i) {

      const unsigned int j = i & (capacity_-1);
      { 
        void (*i)();
        (void)(i);
        if (occupancy_[j]) { 
          if (item == data_[j]) { return capacity_; }
          continue;
        }
       
        if (size_ * MAX_LOAD >= capacity_) { return -1; }
        
        ++size_;
        occupancy_[j] = true;
        data_[j] = item;
        return j;
      }
    }
    assert (MAX_LOAD * (size_ - 1) < capacity_);
    return -1;
  }

  bool HardInsert(const int item) {
    if (-1 != EasyInsert(item)) { return true; }
    
    RandomIntHash that(capacity_ * 2);
    for (unsigned int i = 0; i < capacity_; ++i) {
      if (occupancy_[i]) {
        const int res = that.EasyInsert(data_[i]);
        assert((-1 != res) && "Failed insert on resize");
      }
    }
    that.EasyInsert(item);
    swap_with(that);
    return false;
  }

  void insert(const int item) { HardInsert(item); }

  ~RandomIntHash() {
    INT_ALLOC.deallocate(data_, capacity_);
    //occupancy_.~BoolArray();
  }

  unsigned int size() const { return size_; }
  unsigned int capacity() const { return capacity_; }  

  const int * get_nth(const unsigned int i) const {
    if (occupancy_[i]) {return &data_[i];}
    return nullptr;
  }

public:

  void swap_with(RandomIntHash & that) {
    std::swap(capacity_,that.capacity_);
    std::swap(size_,that.size_);
    std::swap(data_,that.data_);
    occupancy_.swap_with(that.occupancy_);
  }

  void reset() {
    INT_ALLOC.deallocate(data_, capacity_);
    capacity_ = 4*capacity_;
    size_ = 0;
    occupancy_.reset(capacity_);
    data_ = INT_ALLOC.allocate(capacity_);
  }

  vector<int> sorted() const {
    vector<int> ans;
    for (unsigned int i = 0; i < capacity_; ++i) {
      const int * const x = get_nth(i);
      if (nullptr == x) { continue; }
      ans.push_back(*x);
    }
    sort(ans.begin(), ans.end(), less<int>());
    return ans;
  }

  bool same_set(const std::unordered_set<int> & that) const {
    vector<int> ans;
    for (auto x : that) {
      ans.push_back(x);
    }
    sort(ans.begin(), ans.end(), less<int>());
    return sorted() == ans;
  }

private:
  unsigned int capacity_;
  unsigned int size_;
  int * data_;
  struct BoolArray {
  public:
    BoolArray(const unsigned int size) : data_(new bool[size]()) {}
    bool & operator[](const unsigned int n) { return data_[n]; }
    const bool & operator[](const unsigned int n) const { return data_[n]; }
    ~BoolArray() { delete[] data_; }
    void swap_with(BoolArray & that) { std::swap(data_, that.data_); }
    void reset(const unsigned int capacity) { this->~BoolArray(); data_ = new bool[capacity](); }
  private:
    bool * data_;    
  };
  BoolArray occupancy_;

  // MAX_LOAD is the number of slots there must be for every data item

  
  static std::allocator<int> INT_ALLOC;

public:
  const static unsigned int MAX_LOAD = 2;

};

/*static*/ std::allocator<int> RandomIntHash::INT_ALLOC = std::allocator<int>();


#endif
