#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <cstdlib>

#include <iostream>
#include <vector>
#include <unordered_set>
#include <iomanip>
#include <set>

using namespace std;

#include <sparsehash/dense_hash_set> // or sparse_hash_set, dense_hash_map, ...


#include "RandomIntHash.hpp"
#include "timing.hpp"
#include "SmoothRandomIntHash.hpp"
#include "SuccinctRandomIntHash.hpp"

template<typename H, typename T>
void insert_test(const vector<T> & to_insert, const vector<size_t> & places_to_test, vector<double> & test_results) {
  size_t result_num = 0;
  const size_t last_test = places_to_test.back();
  assert (test_results.size() == places_to_test.size());
  assert (to_insert.size() > last_test);
  const auto start_time = get_time();
  H subject;
  //RandomIntHash subject;
  //SuccinctRandomIntHash subject;

  double last_max = 0.0;
  
  //google::dense_hash_set<int> subject;
  //subject.set_empty_key(0);
  for (size_t count = 0; count <= last_test; ++count) {
    const auto start_time = get_time();
    subject.insert(to_insert[count]);
    const auto timing = get_time() - start_time;
    if (timing > last_max) {
      last_max = timing;
      //cerr << "op: \t" << count << endl;
    }

    if (count == places_to_test[result_num]) {
      test_results[result_num++] = last_max;
    }
  }
  //const auto foo = subject./*smaller_.*/get_nth(100000);
  //cerr << (foo ? *foo : -1) << endl;
}

template<typename H, typename T>
double upto_insert_test(const vector<T> & to_insert) {
  static const unsigned many = 15;
  H subjects[many];
  double last_max = 0.0;
  for (size_t i = 0; i < to_insert.size(); ++i) {
    const auto start_time = get_time();
    for (unsigned j = 0; j < many; ++j) {
      subjects[j].insert(to_insert[i]);
    }
    const auto timing = (get_time() - start_time)/((double)many);
    if (timing > last_max) {
      last_max = timing;
      //cerr << "op: \t" << i 
      //     << "\tval: \t" << timing << endl;
    }
    //last_max = max(last_max, timing);
  }
  return last_max;
}

vector<size_t> test_every(const size_t n, const size_t upto) {
  static const int fd = open("/dev/urandom", O_RDONLY);
  assert (fd != -1);
  size_t samp;
  vector<size_t> ans;
  for (size_t count = 0; count < upto; ++count) {
    while (sizeof(size_t) != read(fd, &samp, sizeof(size_t)));
    if (0 == (samp % n)) {
      ans.push_back(count);
    }
  }
  return ans;
}

template<typename T>
vector<T> random_bytes(const size_t n) {
  static const int fd = open("/dev/urandom", O_RDONLY);
  assert (fd != -1);
  srand(0);
  vector<T> ans(n);
  T samp;
  for (size_t count = 0; count < n; ++count) {
    while (sizeof(T) != read(fd, &samp, sizeof(T)));
    ans[count] = samp;
    ans[count] = rand();
  }
  return ans;
}

void print_debug_results(const vector<size_t> & places, const vector<double> & values) {
  assert(places.size() == values.size());
  for (size_t i = 0; i < places.size(); ++i) {
    cout << setprecision(10) << setiosflags(ios_base::fixed)
         << places[i] << '\t'
         << values[i] << endl;
  }
}

void print_cumulative_time(const vector<size_t> &, vector<double> values) {
  sort(values.begin(), values.end(), greater<double>());
  double accum = 0.0;
  for (size_t i = 0; i < values.size(); ++i) {
    cout << setprecision(10) << setiosflags(ios_base::fixed)
         << i << '\t'
         << (accum += values[i]) << endl;
  }
}

vector<double> calculate_cumulative_time(const vector<size_t> &, vector<double> values) {
  vector<double> ans(1, 0.0);
  sort(values.begin(), values.end(), greater<double>());
  double accum = 0.0;
  for (size_t i = 0; i < values.size(); ++i) {
    ans.push_back(ans.back() + values[i]);
  }
  //reverse(ans.begin(), ans.end());
  return ans;
}

vector<double> calculate_avg_time(const vector<size_t> &, vector<double> values) {

  sort(values.begin(), values.end(), greater<double>());
  //return values;
  vector<double> ans;
  ans.push_back(values.front());
  for (size_t i = 1; i < values.size(); ++i) {
    ans.push_back(ans.back() + values[i]);
  }
  //return ans;
  for (size_t i = 0; i < ans.size(); ++i) {
    ans[i] = ans[i]/(static_cast<double>(i+1));
  }
  
  //reverse(ans.begin(), ans.end());
  return ans;
}

double avg(const vector<double> & x) {
  double ans = 0.0;
  for (size_t i = 0; i < x.size(); ++i) {
    ans += x[i];
  }
  return ans/x.size();
}

double std_dev(const vector<double> & x) {
  const double mean = avg(x);
  double ans = 0.0;
  for (size_t i = 0; i < x.size(); ++i) {
    ans += pow(x[i] - mean,2);    
  }
  return sqrt(ans/x.size());
}

int main() {
  //const size_t sample_each = 1000;
  const size_t sample_count = 10;
  const size_t upto = 100000;
  const size_t repetitions = 15;
  vector<vector<double> > data;
  vector<size_t> sample_places;
  for (size_t i = 0; i < repetitions; ++i) {
    data.push_back(vector<double>());
    for (size_t j = 0; j < sample_count; ++j) {
      //const size_t thisone = (size_t)round(pow((double)upto, ((double)j)/((double)sample_count)));
      const size_t thisone = (size_t)round(((double)upto * ((double)j))/((double)sample_count));
      //cerr << j << '\t' << thisone << endl;
      if ((j > 0) && (thisone == sample_places[j-1])) {
        continue;
      }
      if (sample_places.empty() or sample_places.back() < thisone) {
        sample_places.push_back(thisone);
      }
      typedef SmoothRandomIntHash<SuccinctRandomIntHash> T;
      //typedef SmoothRandomIntHash<RandomIntHash> T;
      //typedef set<int> T;      
      //typedef unordered_set<int> T;
      const double ans = upto_insert_test<T>(random_bytes<int>(thisone));
      data[i].push_back(ans);
    }
  }

  for (size_t i = 0; i < sample_places.size(); ++i) {
    vector<double> pts;
    for (size_t j = 0; j < repetitions; ++j) {
      pts.push_back(data[j][i]);
    }
    const double mean = 10000000 * avg(pts);
    const double sig  = 10000000 * std_dev(pts);
    cout << sample_places[i] << '\t'
         << mean - 2*sig << '\t'
         << mean << '\t'
         << mean + 2*sig << endl;
  }

  return 0;
#if 0
  for (size_t i = 0; i < repetitions; ++i) {
    const vector<size_t> samples = test_every(sample_each,upto);
    vector<double> data1(samples.size()), data2(samples.size()), data3(samples.size());
    
    insert_test<RandomIntHash>(random_bytes<int>(upto), samples, data1);
    print_debug_results(samples, data1);

    //insert_test<SmoothRandomIntHash<RandomIntHash> >(random_bytes<int>(upto), samples, data2);
    //print_debug_results(samples, data2);

    //insert_test<SmoothRandomIntHash<SuccinctRandomIntHash> >(random_bytes<int>(upto), samples, data3);
    //print_debug_results(samples, data3);

    /*
    //print_cumulative_time(samples, data);
    const vector<double> 
      res1(calculate_avg_time(samples, data1)), 
      res2(calculate_avg_time(samples, data2)), 
      res3(calculate_avg_time(samples, data3));
    //sort(data1.begin(), data1.end(), greater<double>());
    //sort(data2.begin(), data2.end(), greater<double>());
    //sort(data3.begin(), data3.end(), greater<double>());

    for (size_t j = 0; j < data1.size(); ++j) {
      const auto smallest = 1.0;//min(min(res1[j], res2[j]), res3[j]);
      cout << j << '\t'
           << res1[j]/smallest << '\t'
           << res2[j]/smallest << '\t'
           << res3[j]/smallest << endl;
    }
    */
    
  }
#endif
}

/*

int main() {

  srand(0);

  vector<int> to_insert(1000 * 1000);
  for (unsigned int i = 0; i < to_insert.size(); ++i) {
    to_insert[i] = rand();
  }
 
  const auto begin = get_time();
 
  RandomIntHash ht;

  for (unsigned int i = 0; i < to_insert.size(); ++i) {
    ht.HardInsert(to_insert[i]);
  }
}

*/


// TODO: 1000 data points (evenly and logarithmically distributed), 30
// runs of most expensive operation, plot median and error bars

// TODO: dealloc blocks only on operations in which no other blocks
// have been deallocated

// TODO: trace operations with high cost, try to reduce them

// TODO: deamortize lookups with twisted tabulation hasing

// TODO: take pages and page size into account

// TODO: either better allocation (don't double-store size) or better
// deallocation (don't pass size - unnecessa4y in std::alloc)

// TODO: dealloc contiguous memory regions in pieces by munmapping

// TODO: delete and downsize

// TODO: try representative workloads
//TODO: take into account cache-line size, item size, and page size
