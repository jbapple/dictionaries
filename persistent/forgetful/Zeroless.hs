module Zeroless where

{-

Hist keeps a forgetful history of a counter. It has a certain
characteristic that makes it useful for certain types of partial
persistence: There is a constant c such that for any time n, for any
time i < n, a Hist up to time n will include timestamps p and q such
that p < i < q and i-p < c * min(i,n-i) a and q-i < c * min(i,n-i).

in other words, every time is bracketed by timestamps that are as
close to it as that time is to the beginning of time or now.

This is achieved using a zeroless binary representation of the time n.

-}

data One a = One (N a) (Two a)
           | Stop (N a) (Tail a) deriving (Show)
data Two a = Two (N a) (One a)
           | End (N a) (Tail a) deriving (Show)
data N a = N a [a] deriving (Show)
type Tail a = [a]
data Hist a = A (One a)
            | B (Two a)
            | Zero deriving (Show)
            
consTwo x (End (N y ys) qs) = End (N x (y:ys)) qs
consTwo x (Two (N y ys) zs) = Two (N x (y:ys)) zs

replTwo x (End (N y []) qs) = End (N x []) ((y-1):qs)
replTwo x (End (N _ ys) qs) = End (N x ys) qs
replTwo x (Two (N _ ys) zs) = Two (N x ys) zs

unsnoc x [] = ([],x)
unsnoc x (y:ys) = help y ys [x]
  where help z [] r = (reverse r,z)
        help z (p:ps) r = help p ps (z:r)

pushOne (Stop (N x []) []) = End (N (x+1) []) [x]
pushOne (Stop (N x []) qs) = End (N (x+1) []) qs
pushOne (Stop (N x (y:ys)) qs) = Two (N (x+1) []) (Stop (N y ys) qs)
pushOne (One (N x []) ys) = consTwo (x+1) ys
pushOne (One (N x (y:ys)) zs) = Two (N (x+1) []) (One (N y ys) zs)

pushTwo (End (N x xs) qs) = Stop (N (x+1) (x:xs)) qs
pushTwo (Two (N x []) ys) = One (N (x+1) []) (replTwo x (pushOne ys))
pushTwo (Two (N x (y:ys)) zs) = One (N (x+1) (x:ps)) (replTwo p (pushOne zs))
  where (ps,p) = unsnoc y ys
        
push Zero = A (Stop (N 0 []) [])
push (A x) = B (pushOne x)
push (B y) = A (pushTwo y)

class Flatten t where
  flatten :: t a -> [a]

instance Flatten N where
  flatten (N x xs) = x:xs
  
instance Flatten One where
  flatten (Stop x qs) = flatten x ++ qs
  flatten (One x xs) = flatten x ++ flatten xs
  
instance Flatten Two where
  flatten (End x qs) = flatten x ++ qs
  flatten (Two x xs) = flatten x ++ flatten xs
  
instance Flatten Hist where
  flatten (A x) = flatten x
  flatten (B y) = flatten y
  flatten Zero = []
