#include <stddef.h>

typedef void * (*Getter)(const void *);
typedef void (*Setter)(void *, const void *);
typedef int (*Comparer)(const void *, const void *); // returns which is bigger

struct BstPackage {
  Getter get_left;
  Getter get_right;
  Getter get_key;
  Comparer compare;
};

typedef const struct BstPackage * const Ops;

// returns 1 if kids in order, 0 if reversed, -1 if no kids
int
avl_kids_inorder(const void * const node, Ops ops) {
  if (NULL == node) {
    return -1;
  }

  const void * const node_key = ops->get_key(node);
  const void * const left = ops->get_left(node);
  const void * const right = ops->get_right(node);

  if (NULL == left) {
    if (NULL == right) {
      return -1;
    }
    const void * const right_key = ops->get_key(right);
    if (ops->compare(node_key, right_key) == 1) { // node_key < right_key
      return 1;
    } else {
      return 0; // node_key >= right_key
    }
  }
  
  const void * const left_key = ops->get_key(left);
  if (ops->compare(left_key, node_key) >= 0) { // left_key <= node_key
    return 1;
  } else {
    return 0; // left_key > node_key
  }
} 

void * avl_get_true_left(void * node, Ops ops) {
  if (avl_kids_inorder(node, ops)) {
    return ops->get_right(node);
  }
  return ops->get_left(node);
}


/*
int 
avl_get_balance(const void * const node, const Getter get_left, 
                const Getter get_right, const Getter get_key,
                const Comparer compare) {
  if (NULL == node) {
    return 0;
  }
  
  const void * const node_key = get_key(node);
  const void * const left = get_left(node);
  const void * const right = get_right(node);

  if (NULL == left && NULL == right) {
    return 0;
  }

  if (NULL == left) {
    const void * const right_key = get_key(right);
    if (compare(node_key, right_key) == 1) {
      return 1;
    } else {
      return -1;
    }
  }

  if (NULL == right) {
    const void * const left_key = get_key(left);
    if (compare(node_key, right_key) == 1) {
      return 1;
    } else {
      return -1;
    }


}
*/
