#include <cstdlib>
#include <limits>

template<typename T, unsigned N = 0>
struct faillocator {
  
private:
  struct pet {};

  

public:
  void method() {
    throw pet();
  }

  template<typename U>
  struct rebind {
    typedef faillocator<T, N+1> other;
  };

  typedef T* pointer;
 
  typedef T value_type;
  typedef T & reference;
  typedef const T & const_reference;

  struct const_pointer {
    pointer m_payload;
  private:
    const_pointer(pointer payload) : m_payload(payload) {}
    const_reference operator*() const {return *m_payload;}
    const T * operator->() const {return m_payload;}
  };


  //typedef void void_pointer;
  //typedef void const_void_pointer;

  template<typename U>
  pet destroy(U * p) {
    p->~U();
  }

  typedef unsigned char size_type;
  
  pet deallocate(pointer p, const size_type) {
    free(p);
  }

  pointer allocate(const size_type n) {
    return reinterpret_cast<pointer>(malloc(n * sizeof(T)));
  }

  template<typename U, unsigned M>
  bool operator==(faillocator<U,M> & that) {
    return true;
  }

  template<typename U, unsigned M>
  bool operator!=(faillocator<U,M> & that) {
    return not (*this == that);
  }
  
  size_type max_size() const {
    return std::numeric_limits<size_type>::max();
  }

  template<class... Args>
  pet construct(pointer p, Args&&... args) {
    ::new ((void*)p) T(forward<Args>(args)...);
  }

};
