#include <ctime>
#include <cstdlib>

#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>

using namespace std;

#include "sloppy-interpolation.hpp"

struct seed_rand_t {
  seed_rand_t() {
    const time_t seed = time(NULL);
    cout << "seed = " << seed << endl;
    srand(seed);
  }
} const seed_rand;

vector<unsigned long long> rand_vector(const unsigned n) {
  vector<unsigned long long> ans;
  for (unsigned i = 0; i < n; ++i) {
    const unsigned long long a = rand();
    const unsigned long long b = rand();
    ans.push_back((a << (4*sizeof(unsigned long long))) + b);
  }
  sort(ans.begin(), ans.end());
  return ans;
}

int main(const int argc, char const * const * const argv) {
    
  if (2 != argc) {
    cerr << "Please supply a length" << endl;
    return 1;
  }

  istringstream s(argv[1]);
  unsigned length;
  s >> length;

  const vector<unsigned long long> haystack = rand_vector(length);
  const unsigned long long needle1 = rand();
  const unsigned long long needle2 = rand();
  const unsigned long long needle = (needle1 << (4*sizeof(unsigned long long))) + needle2;
  cout << needle << endl;
  if (needle < haystack[0]) {
    cout << "no predecessor" << endl;
  } else {
    const unsigned long long result = *sloppy_interpolation_predecessor(haystack.begin(), haystack.end(), needle);
    cout << result << endl;
  }
}
