#include <cassert>

template<typename T>
T
sloppy_interpolation_predecessor(T begin, 
				 T end,
				 const unsigned long long key) {
  if (end == begin) {
    return begin;
  }
  if (begin + 1 == end) {
    return begin;
  }
  const unsigned long long & front = *begin;  
  const unsigned long long & back = *(end - 1);
  assert (key >= front);
  if (key == front) {
    return begin;
  }
  if (key >= back) {
    return end-1;
  }

#ifndef NDEBUG
  int iterations = 0;
#endif

  while (true) {
    const unsigned long long & front = *begin;  
    const unsigned long long & back = *(end - 1);


    const unsigned long long bits = 8*sizeof(unsigned long long) - __builtin_clzll(back-front);
    //const unsigned long long up = 8*sizeof(unsigned long long) - __builtin_clzll(end-begin);
    //const unsigned long long count = (key - front) >> (bits - up);
    //const unsigned long long count = ((end - begin) * (key - front)) >> bits;
    const unsigned long long count = (((key - front) >> 32) * (end - begin)) >> (bits - 32);


    T middle = begin + count;
    
#ifndef NDEBUG
    const unsigned long long exact_count = (((key - front) >> 32) * (end - begin)) / ((back-front) >> 32);
    ++iterations;
    cout << "iteration " << iterations << endl
	 << "front = " << front << endl
	 << "back =  " << back << endl
	 << "key =   " << key << endl
	 << "bits =  " << bits << endl
      //<< "up  =   " << up <<  endl
	 << "count = " << count << endl
	 << "exact = " << exact_count << endl
	 << "dist =  " << end-begin << endl;
    assert (((count+1) <= (2 * (exact_count + 1)))
	    and
	    ((exact_count + 1) <= (2 * (count + 1))));
#endif

    
    if (middle <= begin) {
      middle = begin + 1;
    }
    if (middle >= end) {
      middle = end - 1;
    }
    
    
    /*
    if (middle <= begin) {
      middle = begin + 1;//(end - begin)/2;
    }
    if (middle >= end) {
      middle = begin + (end - begin)/2;
    }
    */

    if (key == *middle) {      
      return middle;
    } else if (key < *middle) {
      end = middle;
    } else {
      begin = middle;
    }
    if (end == begin) {
      return begin;
    }
    if (begin + 1 == end) {
      return begin;
    }
  }
}
