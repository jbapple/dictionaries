/* Hash tables can take a long time to modify when they must be
   rebuilt. This one aims to reduce that time by using incremental
   global rebbuilding and depth-k trees (rather than arrays) to
   achieve lookup time of O(k) and worst-case memory allocation time
   of O(n^(1/k))
*/

#include <unordered_map>

template<typename Key, typename Val, typename Hash = std::hash<Key> > //, typename Alloc = allocator<pair<const Key, Val> > > // TODO
struct deamortized_hash_table {

  typedef std::size_t size_t;

  template<typename T>
  struct wide_tree {
    size_t m_log_root_capacity;
    size_t m_log_leaf_capacity;
    size_t m_leaf_count;
    T ** m_root;

    size_t capacity() const {
      return (1ull << (m_log_leaf_capacity + m_log_root_capacity));
    }

    // TODO: new actually touches its memory. We need to use malloc.

    // PROBLEM: what to do about online writes

    // IDEA: just keep them in a list, then purge the list when all of the leaves have been constructed

    wide_tree(const size_t log_root_capacity, 
	      const size_t log_leaf_capacity) : 
      m_log_root_capacity(log_root_capacity),
      m_log_leaf_capacity(log_leaf_capacity),
      m_leaf_count(0),
      m_root(new T*[1ull << m_log_root_capacity]) {

      // TODO: what if throw
      // TODO: uninitializaed?
      m_root[m_leaf_count] = new T[1ull << m_log_leaf_capacity];
      ++m_leaf_count;
    }

    wide_tree(wide_tree && that) :
      m_log_root_capacity(that.m_log_root_capacity),
      m_log_leaf_capacity(that.m_log_leaf_capacity),
      m_leaf_count(that.m_leaf_count),
      m_root(that.m_root) {
      
      that.m_root = 0;
    }

    ~wide_tree() {
      if (0 != m_root) {
	for (size_t i = 0; i < m_leaf_count; ++i) {
	  for (size_t j = 0; j < (1ull << m_log_leaf_capacity); ++j) {
	    m_root[i][j].~T();
	  }
	  delete m_root[i];
	  m_root[i] = 0;
	}
      }
    }

    T & operator[](const size_t i) {
      if (m_leaf_count < (1ull << m_log_root_capacity)) {
	if (0 == m_root[m_leaf_count]) {
	  m_root[m_leaf_count] = new T[1ull << m_log_leaf_capacity];
	}
	++m_leaf_count;
      }
      const size_t p = i >> m_log_leaf_capacity;
      const size_t q = i-(p << m_log_leaf_capacity);
      if (0 == m_root[p]) {
	m_root[m_leaf_count] = new T[1ull << m_log_leaf_capacity];
	++m_leaf_count;
      }
      return m_root[p][q];
    }

    const T & operator[](const size_t i) const {
      const size_t p = i >> m_log_leaf_capacity;
      const size_t q = i-(p << m_log_leaf_capacity);
      return m_root[p][q];
    }
  };

  double m_max_load;
  size_t m_size;

  struct Cell {
    Key key;
    Val val;

    enum State {
      FULL, EMPTY, DELETED
    } state;

    Cell() : key(), val(), state(EMPTY) {}
  };

  wide_tree<Cell> m_smaller, m_bigger, m_just_right;

  deamortized_hash_table() :
    m_max_load(1.0/3.0), m_size(0), m_smaller(0,0), m_bigger(1,1), m_just_right(0,1) {}

  // TODO: delete
  // TODO: lookup

  void grow() {
    // TODO: this is ugly
    m_smaller.~wide_tree<Cell>();
    new (&m_smaller) wide_tree<Cell>(std::move(m_just_right));
    //m_just_right.~wide_tree<Cell>();
    new (&m_just_right) wide_tree<Cell>(std::move(m_bigger));
    if (m_just_right.m_log_leaf_capacity > m_just_right.m_log_root_capacity) {
      new (&m_bigger) wide_tree<Cell>(m_just_right.m_log_root_capacity + 1, m_just_right.m_log_leaf_capacity);
    } else {
      new (&m_bigger) wide_tree<Cell>(m_just_right.m_log_root_capacity, m_just_right.m_log_leaf_capacity + 1);
    }
  }

  // TODO: shrink
  
  static void place(wide_tree<Cell> & table, const Key & key, const Val & val, const size_t location) {
    size_t i = location;
    for (; Cell::FULL == table[i % table.capacity()].state; ++i);
    table[i % table.capacity()].key = key;
    table[i % table.capacity()].key = key;
  }

  void insert(const Key & key, const Val & val) {
    place(m_smaller, key, val, Hash()(key));
    place(m_just_right, key, val, Hash()(key));
    place(m_bigger, key, val, Hash()(key));
    if (m_just_right.capacity() * m_max_load < m_size) {
      grow();
    }

  }
    

  // only valid if key is not already present
  //void insert_new(const Key & key, const Val & val) {
    
};
