#include <iostream>
#include <vector>
#include <cmath>
#include <cassert>

using namespace std;

#include <cstdint>

const int bitlen = 20;

vector<int> sqrt_floors;
uint64_t top_floors[1ull << (bitlen/2)];

int lookup_sqrt_uncorrected(const int x) {
  assert (x < (1 << 16));
  const int top_bits = (x >> 8) & ((1 << 8) - 1);
  if (top_bits > 0) {
    return sqrt_floors[top_bits] << 4;
  }
  const int bottom_bits = x & ((1 << 8) - 1);
  return sqrt_floors[bottom_bits];
}

int lookup_sqrt_plus(const int x) {
  assert (x < (1 << 16));
  const int top_bits = (x >> 8) & ((1 << 8) - 1);
  const int bottom_bits = x & ((1 << 8) - 1);
  if (top_bits == 0) {
    return sqrt_floors[bottom_bits];
  }  
  return (sqrt_floors[top_bits] << 4) + sqrt_floors[bottom_bits];
}

int lookup_sqrt_qdrt(const int x) {
  assert (x < (1 << 16));
  const int top_bits = (x >> 8) & ((1 << 8) - 1);
  const int bottom_bits = x & ((1 << 8) - 1);
  if (top_bits == 0) {
    return sqrt_floors[bottom_bits];
  }  
  return (sqrt_floors[top_bits] << 4) + sqrt_floors[sqrt_floors[bottom_bits]];
}

int lookup_sqrt_qdrt_corr(const int x) {
  assert (x < (1 << 16));
  const int top_bits = (x >> 8) & ((1 << 8) - 1);
  const int bottom_bits = x & ((1 << 8) - 1);
  if (top_bits == 0) {
    return sqrt_floors[bottom_bits];
  }  
  return (sqrt_floors[top_bits] << 4) + sqrt_floors[bottom_bits] - sqrt_floors[sqrt_floors[bottom_bits]];
}

int lookup_sqrt_guess(const int x) {
  assert (x < (1 << 16));
  const int top_bits = (x >> 8) & ((1 << 8) - 1);
  const int bottom_bits = x & ((1 << 8) - 1);
  if (top_bits == 0) {
    return sqrt_floors[bottom_bits];
  }  
  return (sqrt_floors[top_bits] << 4) + sqrt_floors[bottom_bits] - sqrt_floors[lookup_sqrt_uncorrected(top_bits * bottom_bits)];
}

int lookup_sqrt_newton(const int x) {
  assert (x < (1 << 16));
  const int top_bits = (x >> 8) & ((1 << 8) - 1);
  const int bottom_bits = x & ((1 << 8) - 1);
  if (top_bits == 0) {
    return sqrt_floors[bottom_bits];
  }
  const int r = sqrt_floors[top_bits] << 4;
  return r - (r*r - x)/(2*r);
}

int lookup_sqrt_overlap(const int x) {
  assert (x < (1 << 16));
  const int top_bits = (x >> 8) & ((1 << 8) - 1);
  if (top_bits > 0) {
    if (top_bits >= (1 << 4)) {
      return sqrt_floors[top_bits] << 4;
    } else {
      return sqrt_floors[(x >> 4)] << 2;
    }      
  }
  const int bottom_bits = x & ((1 << 8) - 1);
  return sqrt_floors[bottom_bits];
}

int lookup_sqrt_clz(const int x) {
  assert (x < (1 << 16));
  const int bits = sizeof(int)*8 - 1 - __builtin_clz(x);
  if (bits < 8) {
    return sqrt_floors[x];
  }
  const int to_shift = 2 * (bits/2 - 3);
  return sqrt_floors[x >> to_shift] << (to_shift/2);
}

template<int n>
unsigned long long lookup_sqrt_top(const unsigned long long x) {
  assert (0 == (n & 3));
  assert (x < (1ull << n));
  if (0 == x) return 0;
  const int bits = sizeof(unsigned long long)*8 - __builtin_clzll(x);
  if (bits >= (n/2 - 1)) {
    const int shift = 2*((bits-(n/2 - 1))/2);
    return top_floors[(x >> shift) - (1ull << (n/2 - 2))] >> (((n/2) - shift)/2);
  }
  const int shift = 2*(((n/2)-bits)/2);
  return top_floors[(x << shift) - (1ull << (n/2 - 2))] >> (shift/2 + (n/4));
}

template<int n>
unsigned long long lookup_sqrt(const unsigned long long x) {
  const unsigned long long ans = lookup_sqrt_top<n>(x);
  if ((ans+1)*(ans+1) <= x) {
    return ans+1;
  }
  return ans;
}


//unsigned long long (*lookup_sqrt)(const int, const unsigned long long) = &linear_lookup_sqrt_top;

#include <sys/times.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

double get_time() {
  static const double freq = static_cast<double>(1000*1000*1000);
  static timespec atime;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID,&atime);
  return ((static_cast<double>(atime.tv_sec) * 1000 * 1000 * 1000) + static_cast<double>(atime.tv_nsec))/freq;
  /*
  static const double freq = static_cast<double>(sysconf(_SC_CLK_TCK));
  static tms atime;
  times(&atime);
  return static_cast<double>(atime.tms_utime + atime.tms_stime)/freq;
  */
}

int main() {
  for(int i = 0; i < (1 << 8); ++i) {
    sqrt_floors.push_back(floor(sqrt(i)));
  }
  
  //const int bitlen = 32;
  
  // idea: can use tables of size 2^(w/2 - k) and search time is O(1) + k
  // so: U^epsilon = (2^w)^e = 2^we and if we = w/2 - k, then k = w/2 - we = w(1/2-e)
  // so search time is w(1/2-e)
  
  for(unsigned long long i = (1ull << (bitlen/2 - 2)); i < (1ull << bitlen/2); ++i) {
    top_floors[i-(1ull << (bitlen/2 - 2))] = floor(sqrt(i << bitlen/2));
    if (i > (1ull << (bitlen/2 - 2))) { // idea: only store a bit or two for each sqrt
      const unsigned long long here = floor(sqrt(i << bitlen/2));
      const unsigned long long there = floor(sqrt((i-1) << bitlen/2));
      if (here - there > 1) {
        cout << i << '\t'
             << here << '\t'
             << there << endl;
      }

      // this is a rank problem

      // we could store every other element as a cache and every other element as a popcountable incrs

    }
      // in fact, the values are very close to linear in aggregate: each 1.5 bits in domain gets 1 bit in range
      const unsigned long long loc = i-(1ull << (bitlen/2 - 2));
      const unsigned long long expected = (loc * 2)/3 + (1ull << (bitlen/2 - 1));
      const unsigned long actual = floor(sqrt(i << bitlen/2));

      cout << i << '\t'
           << ((actual > expected) ? (actual - expected) : (expected - actual)) << '\t'
           << actual << '\t'
           << expected << endl;

      // however, they are not linear locally


  }
    //assert(
  
  
  //cout << "begin" << endl;

  const int repeater = 1;

  unsigned long long sum = 0;
  
  double old = get_time();
  for (int j = 0; j < repeater; ++j) {
    for (unsigned long long i = 0; i < (1ull << bitlen); ++i) {
      sum += lookup_sqrt<bitlen>(i);
    }
  }
  cout << sum << endl;
  cout << get_time() - old << endl;
  
  old = get_time();
  for (int j = 0; j < repeater; ++j) {
    for (unsigned long long i = 0; i < (1ull << bitlen); ++i) {
      sum += floor(sqrt(i));
    }
  }
  cout << sum << endl;
  cout << get_time() - old << endl;
  return 0;

  /*
  long littlest_diff = 0;
  long biggest_diff = 1;
  bool print = false;
  for (unsigned long long i = 0; i < (1ull << bitlen); ++i) {
    const long diff = floor(sqrt(i)) - lookup_sqrt(bitlen,i);
    if (diff > biggest_diff) {
      biggest_diff = diff;
      print = true;
    }
    if (diff < littlest_diff) {
      littlest_diff = diff;
      print = true;
    }
    if (print) {
      cout << i << '\t'
           << lookup_sqrt(bitlen,i) << '\t'
           << floor(sqrt(i)) << endl;
      print = false;
    }
  }
  */
}
