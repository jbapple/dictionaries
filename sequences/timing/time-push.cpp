#include <stdio.h>

#include <cassert>

#include <vector>
#include <deque>
#include <iostream>
#include <string>

using namespace std;

#include <boost/lexical_cast.hpp>

using namespace boost;

#include <sys/times.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#include <folly/FBVector.h>

#include "amortized_succinct_vector.hpp"
#include "sqrt_vector.hpp"

double get_time() {
  static const double freq = static_cast<double>(1000*1000*1000);
  static timespec atime;
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID,&atime);
  return ((static_cast<double>(atime.tv_sec) * 1000 * 1000 * 1000) + static_cast<double>(atime.tv_nsec))/freq;
  /*
  static const double freq = static_cast<double>(sysconf(_SC_CLK_TCK));
  static tms atime;
  times(&atime);
  return static_cast<double>(atime.tms_utime + atime.tms_stime)/freq;
  */
}

unsigned long get_size() {
  static const string where = string("/proc/") + lexical_cast<string>(getpid()) + "/statm";
  FILE * const asc = fopen(where.c_str(), "r");
  unsigned long ans;
  fscanf(asc, "%lu", &ans);
  fclose(asc);
  return ans;
}

vector<bool> random_bits(const size_t many) {
  static const int fd = open("/dev/urandom", O_RDONLY);
  char x[2];
  vector<bool> ans(many);
  for (size_t i = 0; i < many; ++i) {
    read(fd, &x, 2);
    ans[i] = (0 == x[0]) && (0 == (x[1] & 63));
  }
  return ans;
}

struct snapshot {
  size_t size;
  double time;
  unsigned long mem;
  double iter_time;
};

template<typename T>
vector<snapshot> test_pb(const size_t n) {
  const vector<bool> where = random_bits(n);
  size_t k = 0;
  for (size_t i = 0; i < n; ++i) {
    if (where[i]) ++k;
  }
  vector<snapshot> ans(k);
  //const double time_begin = get_time();
  //const unsigned long space_begin = get_size();
  T dummy;
  size_t j = 0;
  for (size_t i = 0; i < n; ++i) {
    if (where[i]) {
      ans[j].size = i;
      //ans[j].time = get_time() - time_begin;
      //ans[j].mem = get_size() - space_begin;
            
      const double iter_time_begin = get_time();
      size_t ok = 0;
      for (size_t k = 0; k < i; ++k) {
        for (size_t m = 0; m < 100; ++m) {
          ok += dummy[((k^(m*m)) + i)%i];
        }
      }
      cerr << ok << endl;
      ans[j].iter_time = get_time() - iter_time_begin;
      
      ++j;
    }
    dummy.push_back(n);
  }
  return ans;
}



template<typename T>
void print_pairs(const vector<T> x) {
  cout.setf(ios::fixed, ios::floatfield);
  cout.setf(ios::showpoint);
  cout.precision(9);

  for (size_t i = 0; i < x.size(); ++i) {
    cout << x[i].size << '\t' 
      //<< x[i].time << '\t' 
      //<< x[i].mem << '\t' 
      << x[i].iter_time 
         << endl;
    //malloc_stats();
  }
}


int main(int argc, char ** argv) {
  assert (2 == argc);
  const size_t n = lexical_cast<size_t>(argv[1]);  
  print_pairs(test_pb<srv<int> >(n));
}
